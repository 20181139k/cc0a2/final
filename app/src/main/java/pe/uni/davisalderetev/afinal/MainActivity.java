package pe.uni.davisalderetev.afinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import com.bosphere.filelogger.FL;

public class MainActivity extends AppCompatActivity {

    Button buttonNext,buttonAnt,buttonTrue,buttonFalse;
    TextView textViewQues;
    ImageView imageView;
    LinearLayout linearLayout;
    ArrayList<String> text=new ArrayList<>();
    ArrayList<String> text1=new ArrayList<>();
    ArrayList<Integer> image=new ArrayList<>();
    int size,count=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonNext=findViewById(R.id.button_next);
        buttonAnt=findViewById(R.id.button_ant);
        buttonTrue=findViewById(R.id.button_true);
        buttonFalse=findViewById(R.id.button_false);
        textViewQues=findViewById(R.id.txt_pregun);
        imageView=findViewById(R.id.img_figure);

        FL.i("Recursos img, preguntas y respuestas de la clase prueba");
        prueba p = new prueba(this);
        image=p.getImages();
        text=p.getPre();
        text1=p.getRes();
        size=text.size();
        FL.i("configurando recursos");
        imageView.setImageResource(image.get(count));
        textViewQues.setText(text.get(count));

        buttonNext.setOnClickListener(v -> {
            FL.i("cambiando de pregunta next");
            count=count+1;
            if (count<size) {
                imageView.setImageResource(image.get(count));
                textViewQues.setText(text.get(count));
            }else{
                count=0;
                imageView.setImageResource(image.get(count));
                textViewQues.setText(text.get(count));
            }


        });

        buttonAnt.setOnClickListener(v -> {
            count=count-1;
            FL.i("cambiando de pregunta anterior");
            if(0<count){
                imageView.setImageResource(image.get(count));
                textViewQues.setText(text.get(count));
            }else{
                count=0;
                imageView.setImageResource(image.get(count));
                textViewQues.setText(text.get(count));
            }

        });

        buttonTrue.setOnClickListener(v -> {
            FL.i("Boton de verificacion Verdadero");
            Snackbar.make(linearLayout,text1.get(count),Snackbar.LENGTH_LONG).show();
        });

        buttonFalse.setOnClickListener(v -> {
            FL.i("Boton de verifiacion Falso");
            Snackbar.make(linearLayout,text1.get(count),Snackbar.LENGTH_LONG).show();
        });


    }
}